import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$:Observable<firebase.User>;

  constructor(private aF:AngularFireAuth,private route:ActivatedRoute) {
    this.user$=aF.authState;
   }

  login(){
    let returnUrl=this.route.snapshot.queryParamMap.get('returnUrl');
    localStorage.setItem('returnUrl',returnUrl);
    this.aF.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  logout(){
    this.aF.auth.signOut();
  }
}
